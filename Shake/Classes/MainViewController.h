//
//  MainViewController.h
//  AccelerometerTutorial
//
//  Created by Brandon Cannaday on 8/5/09.
//  Copyright 2009 Paranoid Ferret Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import<AVFoundation/AVAudioPlayer.h>

@interface MainViewController : UIViewController <UIAccelerometerDelegate,AVAudioPlayerDelegate> 
{
  AVAudioPlayer* player;
  UIAccelerometer *accelerometer;
}


@property (nonatomic, retain) UIAccelerometer *accelerometer;

@end
