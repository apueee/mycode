//
//  AppDelegate.h
//  test2
//
//  Created by Ayush Goel on 17/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sample/try.h>
@class ViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
  try *obj; 
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
